#!/bin/sh
set -e

# Substitute env variables
TMP_FILE=$(mktemp)
envsubst < /static/index.html > "$TMP_FILE"
cat "$TMP_FILE" > /static/index.html
rm "$TMP_FILE"

# Start nginx
exec "$@"
