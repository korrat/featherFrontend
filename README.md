# Feather frontend

A web application frontend for the lightweight user administration tool named "Feather".

## Project setup

```bash
yarn install
```

### Compiles and hot-reloads for development

The following options can be exported as environment variables before starting the local development environment. If the environment variable is not set, the default value as stated in the table is used.

| Name                       | Description                                    | Default value                                    |
| -------------------------- | ---------------------------------------------- | ------------------------------------------------ |
| VITE_VENDOR_NAME           | The name, describing this instance             | LOCAL DEV                                        |
| VITE_API_BASE_URL          | The base url for the backend                   | http://localhost:7000                            |
| VITE_BASE_URL              | The base url for the frontend                  | http://localhost:3000                            |
| VITE_LOGO_URL              | The url for the logo                           | https://maximilian.dev/assets/images/favicon.svg |
| VITE_MAIN_BAR_COLOR        | The color (as hex) for the main navigation bar | #db3a17                                          |
| VITE_QUICK_START_GUIDE_URL | The url for the quick start guide              | https://maximilian.dev                           |

```sh
yarn serve
```

For example if the backend is running on port 12345:

```sh
VITE_API_BASE_URL=http://localhost:12345 yarn serve
```

### Compiles and minifies for production

```bash
yarn build
```

### Lints and fixes files

```bash
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

## License

Licensed under [Apache 2.0](LICENSE)

## Contribution

Very welcome!

1. [Fork it](https://gitlab.com/ingenieure-ohne-grenzen/featherFrontend/-/forks/new)
1. Create your feature branch (git checkout -b my-new-feature)
1. Commit your changes (git commit -am 'Add some feature')
   - In your first commit, add your name and email address (if you like) to `AUTHORS.md`.
1. Push to the branch (git push origin my-new-feature)
1. Create a new Pull Request
