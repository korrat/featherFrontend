/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import Forgot from '../views/Forgot.vue';
import LoginLayout from '../components/LoginLayout.vue';
import Reset from '../views/Reset.vue';
import Signup from '../views/Signup.vue';
import ChangeMailVerification from '../views/ChangeMailVerification.vue';
import Logout from '../views/Logout.vue';
import Login from '../views/Login.vue';

export default {
  path: '/',
  component: LoginLayout,
  redirect: '/login',
  children: [
    {
      path: 'login',
      name: 'Login',
      component: Login,
    },
    {
      path: 'logout',
      name: 'Logout',
      component: Logout,
    },
    {
      path: 'forgot',
      name: 'Passwort vergessen',
      component: Forgot,
    },
    {
      path: 'reset/:token',
      name: 'Passwort setzen',
      component: Reset,
    },
    {
      path: 'invite/:token',
      name: 'Registrieren',
      component: Signup,
    },
    {
      path: 'change_mail/:token',
      name: 'E-Mail-Adresse bestätigen',
      component: ChangeMailVerification,
    },
  ],
};
