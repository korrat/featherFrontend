export type GdprDocument = {
  id: number;
  creationDate: string;
  forceDate: string;
  validDate: string;
  content: string;
};
