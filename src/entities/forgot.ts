import { User } from './user';

export type PasswordReset = {
  id: string;
  user: User;
  validUntil: string;
};
