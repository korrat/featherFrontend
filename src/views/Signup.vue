<!--
  -    Copyright [2020] Feather development team, see AUTHORS.md
  -
  -    Licensed under the Apache License, Version 2.0 (the "License");
  -    you may not use this file except in compliance with the License.
  -    You may obtain a copy of the License at
  -
  -        http://www.apache.org/licenses/LICENSE-2.0
  -
  -    Unless required by applicable law or agreed to in writing, software
  -    distributed under the License is distributed on an "AS IS" BASIS,
  -    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  -    See the License for the specific language governing permissions and
  -    limitations under the License.
  -->
<script setup lang="ts">
import * as api from '../api';
import * as util from '../util';
import GdprPopup from '../components/GdprPopup.vue';
import { onMounted, ref } from 'vue';
import { BackgroundJob, GdprDocument, Invitation } from '../entities';
import { useRoute } from 'vue-router/composables';

const signUpForm = ref(null);
const route = useRoute();
const checking = ref(true);
const loading = ref(false);
const finalizeInvitationJobId = ref<string | undefined>(undefined);
const success = ref(false);
const error = ref<string[]>([]);
const gdpr = ref<GdprDocument | undefined>(undefined);
const displayGdprPopup = ref(false);
const signUp = ref<{
  password1: string;
  password2: string;
  displayName: string;
  gdpr: undefined | GdprDocument;
}>({
  password1: '',
  password2: '',
  displayName: '',
  gdpr: undefined,
});
const token = ref<Invitation | undefined>(undefined);

onMounted(() => {
  const gdprPromise = api.gdpr.getGdprDocument('latest').then((data) => (gdpr.value = data));
  const invitationPromise = api.invitation
    .getInvitation(route.params.token)
    .then((invitation) => (token.value = invitation));

  api.allCatching([gdprPromise, invitationPromise]).finally(() => (checking.value = false));
});

function doSignupOrGdpr(token: Invitation) {
  // @ts-ignore
  if (!signUpForm.value.validate()) return;

  if (!gdpr.value) {
    doSignup(token);
  } else {
    displayGdprPopup.value = true;
  }
}
function doSignup(withToken: Invitation) {
  loading.value = true;

  const consumeInvitationPromise = api.invitation.consumeInvitation(
    withToken.id,
    signUp.value.password2,
    signUp.value.displayName,
    gdpr.value?.id
  );

  consumeInvitationPromise.then((data) => {
    switch (data.state) {
      case 'success': {
        if (!data.successData) {
          error.value = ['Ein unbekannter Fehler ist aufgetreten'];
          loading.value = false;
        } else {
          finalizeInvitationJobId.value = data.successData?.id ?? '';
        }

        break;
      }
      case 'token_not_found': {
        token.value = undefined;
        loading.value = false;
        break;
      }
      case 'error': {
        error.value = data.messages;
        loading.value = false;
      }
    }
  });

  api.allCatching([consumeInvitationPromise]).catch(() => {
    loading.value = false;
  });
}

function processResults(result: BackgroundJob | api.job.ErrorResult) {
  if (api.job.isErrorResult(result)) {
    error.value = [result.error];
  } else {
    success.value = true;
  }
}
</script>

<template>
  <div v-if="!checking">
    <gdpr-popup
      v-if="!loading && gdpr && token"
      :gdpr="gdpr"
      v-model="displayGdprPopup"
      v-on:declineGdpr="displayGdprPopup = false"
      v-on:acceptGdpr="
        signUp.gdpr = gdpr;
        displayGdprPopup = false;
        doSignup(token);
      "
    ></gdpr-popup>
    <v-card-text>
      <v-alert type="warning" v-if="!token">
        Das Token konnte nicht gefunden werden, eventuell wurde es bereits benutzt oder ist
        abgelaufen.
      </v-alert>

      <v-alert type="success" v-if="success">
        Registrierung abgeschlossen, du kannst dich nun einloggen.
      </v-alert>

      <v-form ref="signUpForm" v-if="token && !success">
        <v-alert type="warning" v-if="error.length > 0">
          Bitte folgende Fehler beheben:
          <ul>
            <li v-for="e in error" :key="e">
              {{ e }}
            </li>
          </ul>
        </v-alert>

        <v-row>
          <v-col>
            <v-text-field
              label="Vorname"
              prepend-icon="mdi-account-box"
              v-model="token.invitee.firstname"
              disabled
            />
          </v-col>
          <v-col>
            <v-text-field
              label="Nachname"
              prepend-icon="mdi-account-box-outline"
              v-model="token.invitee.surname"
              disabled
            />
          </v-col>
        </v-row>

        <v-text-field
          label="E-Mail"
          prepend-icon="mdi-email"
          v-model="token.invitee.mail"
          disabled
        />

        <v-text-field
          label="Anzeigename"
          append-icon="mdi-lightbulb-outline"
          @click:append="signUp.displayName = util.suggestDisplayName(token.invitee)"
          :hint="`Empfehlung: ${util.suggestDisplayName(token.invitee)}`"
          :placeholder="util.suggestDisplayName(token.invitee)"
          prepend-icon="mdi-card-account-details"
          v-model="signUp.displayName"
          :disabled="loading"
          :rules="[util.validateDisplayName]"
        />

        <v-text-field
          label="Passwort"
          name="password1"
          type="password"
          prepend-icon="mdi-lock"
          v-model="signUp.password1"
          :disabled="loading"
          :rules="[util.validatePassword]"
        />

        <v-text-field
          label="Passwort wiederholen"
          name="password2"
          prepend-icon="mdi-lock"
          type="password"
          v-model="signUp.password2"
          :rules="[
            util.validatePassword,
            util.validatePasswordsEqual(signUp.password1, signUp.password2),
          ]"
          :disabled="loading"
        />
      </v-form>
    </v-card-text>
    <v-card-actions>
      <background-job-status
        v-if="finalizeInvitationJobId && loading && !success"
        :job-id="finalizeInvitationJobId"
        @job_completed="processResults"
      >
      </background-job-status>

      <router-link class="deep-orange--text" to="/login" v-if="!loading || success"
        >&lt; Zurück zum Login</router-link
      >
      <v-spacer />
      <v-btn
        color="orange darken-2"
        class="pa-4 white--text"
        tile
        @click="doSignupOrGdpr(token)"
        :loading="loading"
        :disabled="loading"
        v-if="token && !success && !loading"
        >Registrieren
      </v-btn>
    </v-card-actions>
  </div>
  <div class="text-center" v-else>
    <v-progress-circular indeterminate color="primary"></v-progress-circular>
  </div>
</template>
