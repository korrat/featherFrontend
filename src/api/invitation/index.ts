import { Invitation, BackgroundJob } from '../../entities';
import { axios, runCatching } from '../util';

export async function getInvitation(id: string): Promise<Invitation | undefined> {
  return await runCatching<Invitation>(() =>
    axios.get(`/invitations/${id}`, {
      validateStatus: (it) => it == 200 || it == 404 || it == 400,
    })
  ).then((it) => (it.status == 200 ? it.data : undefined));
}

export async function consumeInvitation(
  id: string,
  password: string,
  displayName: string,
  gdprId?: number
): Promise<{
  state: 'success' | 'token_not_found' | 'error';
  messages: string[];
  successData: BackgroundJob | undefined;
}> {
  return await runCatching<string[] | { id: string }>(() =>
    axios.post(
      `/invitations/${id}`,
      {
        password1: password,
        password2: password,
        displayName: displayName,
        gdpr: gdprId,
      },
      {
        validateStatus: (it) => it == 201 || it == 404 || it == 400,
        params: {
          async: true,
        },
      }
    )
  ).then((it) => ({
    state: it.status == 404 ? 'token_not_found' : it.status == 201 ? 'success' : 'error',
    messages: it.status == 400 && Array.isArray(it.data) ? it.data : [],
    successData: it.status == 201 ? (it.data as BackgroundJob) : undefined,
  }));
}
