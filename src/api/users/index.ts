import { axios, runCatching } from '../util';

export type UserResponse = {
  id: number;
  username: String;
  displayName: String;
  firstname: String;
  surname: String;
  mail: String;
  groups: Set<number>;
  registeredSince: string;
  ownedGroups: Set<number>;
  permissions: Set<string>;
  disabled: boolean;
  lastLoginAt: string;
};

export async function getAllUsers(): Promise<UserResponse[]> {
  return await runCatching<UserResponse[]>(() => axios.get('/users')).then((it) => it.data);
}
