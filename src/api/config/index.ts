import { config } from '../../entities';
import { axios, runCatching } from '../util';

export async function getConfig(): Promise<config.Config> {
  return await runCatching<config.Config>(() => axios.get('/config')).then((it) => it.data);
}
