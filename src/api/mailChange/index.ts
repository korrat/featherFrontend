import { MailChange } from '../../entities';
import { axios, runCatching } from '../util';

export async function getMailChange(token: string): Promise<MailChange | undefined> {
  return await runCatching<MailChange>(() =>
    axios.get(`/change_mail/step2/${token}`, {
      validateStatus: (it) => it == 200 || it == 404 || it == 400,
    })
  ).then((it) => (it.status == 200 ? it.data : undefined));
}

export async function executeMailChange(token: string): Promise<{
  state: 'success' | 'token_not_found' | 'error';
  messages: string[];
}> {
  return await runCatching<string[]>(() =>
    axios.post(
      `/change_mail/step2/${token}`,
      {
        /* empty body */
      },
      {
        validateStatus: (it) => it == 204 || it == 404 || it == 400,
      }
    )
  ).then((it) => ({
    state: it.status == 404 ? 'token_not_found' : it.status == 204 ? 'success' : 'error',
    messages: it.status == 400 ? it.data : [],
  }));
}
