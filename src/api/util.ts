import * as Axios from 'axios';
import { CONFIG } from '../util';
import { Alert } from '../entities/alert';
import { useApplicationStore } from '../stores/application';

export const apiBaseUrl = CONFIG.API_BASE_URL;

export type ApiErrorAnswer = {
  reason: 'network' | 'unauthorized' | 'forbidden' | 'server_error' | 'unexpected' | 'unknown';
  explanation: string;
  short: string;
};

export const axios = Axios.default.create({
  baseURL: `${apiBaseUrl}/v1`,
  headers: { Accept: 'application/json' },
  withCredentials: true,
});

export class ApiError extends Error {
  readonly answer: ApiErrorAnswer;
  readonly isApiError: boolean = true;

  constructor(answer: ApiErrorAnswer) {
    super('An api error occured');
    this.answer = answer;
  }

  toAlert(): Alert {
    return {
      type: 'error',
      description: this.answer.explanation,
      short: this.answer.short,
      uniqueType: this.answer.reason,
    };
  }
}

export function isApiError(payload: any): payload is ApiError {
  return payload !== null && typeof payload === 'object' && payload.isApiError === true;
}

export function allCatching(promises: Promise<any>[]): Promise<boolean> {
  return new Promise((resolve, reject) => {
    const applicationStore = useApplicationStore();
    const errors: any[] = [];

    promises.forEach((promise) =>
      promise.catch((error) => {
        if (isApiError(error)) {
          if (error.answer.reason === 'unauthorized') {
            applicationStore.logoutRequested();
          } else {
            applicationStore.pushAlert(error.toAlert());
          }
        } else {
          errors.push(error);
        }
      })
    );
    Promise.all(promises)
      .then(() => resolve(true))
      .catch(() => {
        if (errors.length > 0) {
          reject(errors);
        } else {
          resolve(false);
        }
      });
  });
}

export async function runCatching<T>(
  fun: () => Promise<Axios.AxiosResponse<T>>
): Promise<Axios.AxiosResponse<T>> {
  try {
    return await fun();
  } catch (error) {
    if (Axios.isAxiosError(error)) {
      if (error.response) {
        switch (error.response.status) {
          case 401:
            throw new ApiError({
              reason: 'unauthorized',
              short: 'Nicht angemeldet',
              explanation: 'Benutzer ist nicht angemeldet',
            });
          case 403:
            throw new ApiError({
              reason: 'forbidden',
              short: 'Zugriffsfehler',
              explanation: 'Benutzer darf diese Aktion nicht durchführen',
            });
          case 500:
            throw new ApiError({
              reason: 'server_error',
              short: 'Interner Serverfehler',
              explanation: 'Ein interner Serverfehler ist aufgetreten',
            });
        }
      }

      switch (error.code) {
        case Axios.AxiosError.ERR_NETWORK: {
          throw new ApiError({
            reason: 'network',
            short: 'Netzwerkfehler',
            explanation: 'Ein Netzwerkfehler ist aufgetreten',
          });
        }
      }
    }

    console.log(error);

    throw new ApiError({
      reason: 'unknown',
      short: 'Unbekannter Fehler',
      explanation: 'Ein unbekannter Fehler ist aufgetreten',
    });
  }
}
