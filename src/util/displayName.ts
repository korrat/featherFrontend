import { User } from '../entities';

const regex = RegExp('^[a-z][a-z0-9\\.]*$');

export function validateDisplayName(displayName?: string): string | boolean {
  if (!displayName) return 'Bitte einen Anzeigenamen wählen';
  if (displayName.length < 3) return 'Der Anzeigename muss mindestens 3 Zeichen beinhalten';
  if (displayName.length > 40) return 'Der Anzeigename darf maximal 40 Zeichen beinhalten';
  return regex.test(displayName)
    ? true
    : 'Nur Kleinbuchstaben, Zahlen und Punkte. Darf nur mit einem Buchstaben starten.';
}

function harmonizeName(name: string) {
  let result = name.toLowerCase();
  result = result.replace(/ß/g, 'ss');
  result = result.replace(/ä/g, 'ae');
  result = result.replace(/ö/g, 'oe');
  result = result.replace(/ü/g, 'ue');
  result = result.replace(/ /g, '.');
  result = result.replace(/[^a-z0-9\\.]+/g, '');

  return result;
}

export function suggestDisplayName(user: User): string {
  return `${harmonizeName(user.firstname)}.${harmonizeName(user.surname)}`;
}

export function userIdentifier(user: User, viewingUser?: User) {
  // NOTE: never show self-chosen <username>
  if (
    viewingUser &&
    (viewingUser.permissions.includes('ADMIN') || viewingUser.ownedGroups.length !== 0)
  ) {
    return user.displayName !== `${user.firstname} ${user.surname}`
      ? `${user.firstname} ${user.surname} (${user.displayName})`
      : user.displayName;
  } else {
    return user.displayName;
  }
}

export function userUniqueIdentifier(user: User): string {
  return user.displayName + ' (' + user.username + ')';
}
